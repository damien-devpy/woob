Woob session importer
=====================

This is a webextension to export a woob session URL and cookies and load it in
a real browser.

Build
-----

The extension can be temporarily loaded by loading "manifest.json" in
about:debugging in Firefox and chrome://extensions/ in Chromium.

To build a package, `web-ext build` can be used.
(See https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext)

Use
---

To export the session with woob-debug for example:

>>> print(json.dumps(browser.export_session()))

Copy the JSON output (sample):

{"url": "https://example.com/foo", "cookies": [{"name": "foo", "value": "bar"}]}

Then click on the Woob toolbar button of your browser and paste the JSON.
The browser will set the cookies from woob and go to the same URL.
